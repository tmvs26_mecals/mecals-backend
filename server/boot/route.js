var dsConfig = require('../datasources.json');
var path = require('path');
'use strict';

module.exports = function (app) {
  var User = app.models.Worker;
  var Shift = app.models.Shift;
  var Failure = app.models.Failure;

  //log a user in
  app.post('/login', function (req, res) {
    User.login({
      email: req.body.email,
      password: req.body.password,
      ttl: 60 * 60 * 24
    }, 'user', function (err, accesstoken) {
      if (err) {
        // Log failure into Failure table
        Failure.create({
          worker_id: "new_user",
          type: "Login",
          time: new Date()
        }, function (err, failure) {
          if (err) throw err;
          console.log('Failure logged', failure);
        });

        res.status(401)
        res.json({
          error: "Login failed!",
          error_status: 401
        })
        return;
      } else {
        res.status(200)
        res.json({
          accesstoken: accesstoken
        })
      }
    });
  })

  //log a user out
  app.get('/logout', function (req, res, next) {
    if (!req.query.accessToken) return res.sendStatus(401);
    User.logout(req.query.accessToken, function (err) {
      if (err) return next(err);
      res.redirect('/');
    });
  });

  //Workstation login 
  app.post('/asmwsLogin', function (req, res) {

    // Get user id and user role
    User.findOne({ where: { email: req.body.email } }, (err, user) => {
      // Check if the user exists in database
      if (err || !user) {
        // Log into Failure table
        // <<code here>> ??

        // Set response
        res.status(404)
        res.json({
          error: "The user '" + req.body.email + "' does not exist!",
          error_status: 404
        })
        return;
      } else {

        user.shift(function (err, shift) {
          if (err || !shift) {
            console.log('Something went wrong')
            console.log(err)
            //Check if the role assigned to the userfrom 'Shift' table is similar with user role from 'Worker'table
          } else if (user.role != shift.role) {
            // Log failure into Failure table
            Failure.create({
              worker_id: user.id,
              type: "Assigned role",
              time: new Date()
            }, function (err, failure) {
              if (err) throw err;
              console.log('Failure logged', failure);
            });

            res.status(400)
            res.json({
              error: "The user '" + req.body.email + "' does not have '" + shift.role + "' role assigned!",
              error_status: 400
            })
            return;
          }
          shift.asmws(function (err, asmws) {
            if (asmws.id != req.body.asmws_id) {
              // Check if the user has assigned a specific workstaion
              console.log(shift.asmws_id)
              console.log(req.body.asmws_id)
              // Log into Failure table
              Failure.create({
                worker_id: user.id,
                type: "Assigned asmws",
                time: new Date()
              }, function (err, failure) {
                if (err) throw err;
                console.log('Failure logged', failure);
              });

              console.log(req.body.asmws_id)
              res.status(400)
              res.json({
                error: "Check scheduled shift!",
                error_status: 400
              })
              return;
            }
            else {
              console.log(user.id)

              // Set start_time in Shift table to mark that the shift has started
              shift.updateAttribute(
                "start_time",
                new Date,
                function (err, obj) {
                  if (err) {
                    console.log(err);
                  }
                }
              );
              res.status(200)
              res.json({
                Success: "Succesful connection!"
              })
            }
          });
        })
      }
    });
  });

  app.get('/waitingRoleValidation', function (req, res) {
    if (typeof req.query.email === 'undefined'
      || typeof req.query.requestedRole === 'undefined') {
      res.status(400)
      res.json({
        error: "Request query is not defined properly!",
        error_status: 400
      })
      return;
    }
    User.findOne({ where: { email: req.query.email } }, (err, user) => {
      if (err || !user) {
        // Set response
        res.status(404)
        res.json({
          error: "The user '" + req.query.email + "' does not exist!",
          error_status: 404
        })
        return;
      } else {
        user.shift(function (err, shift) {

          if (err || !shift) {
            console.log(err)
            res.status(500)
            res.json({
              error: "Shift error",
              error_status: 500
            })
          } else if (shift.role === req.query.requestedRole) {
            console.log("HERE 1")
            res.json({})
            return res.status(204).send();
          } else {
            console.log("HERE 2")
            res.json({})
            return res.status(202).send();
          }
        });
      }
    });
  })

  // Change role of ATCO
  app.post('/changeRole', function (req, res) {

    if (typeof req.body.email == 'undefined' || typeof req.body.new_role == 'undefined') {
      res.status(400)
      res.json({
        error: "Request body is not defined properly!",
        error_status: 400
      })
      return;
    }

    User.findOne({ where: { email: req.body.email }, includes: 'scheduled_by' }, (err, user) => {
      console.log(req.body.email);
      if (err || !user) {
        // Set response
        res.status(404)
        res.json({
          error: "The user '" + req.body.email + "' does not exist!",
          error_status: 404
        })
        return;
      } else {
        user.shift(async function (err, shift) {
          try {
            if (err || !shift) {
              console.log('Error when retrieving shift from worker')
              console.log(err)
              throw err
            } else if (shift.role !== req.body.new_role) {
              shiftmanager = await User.findOne({ where: { id: shift.scheduled_by } })
              if (!shiftmanager) {
                throw "Shift Manager not found"
              }
              shiftmanager.NotificationsManager.create(shiftmanager.id, user.id, user.email, req.body.new_role)
              res.status(201)
              res.json({
                validationURL: '/waitingRoleValidation',
                Success: 'Change role request send to shift manager'
              });

            } else {
              user.updateAttribute('role', req.body.new_role, function (err, instance) {
                if (err || !instance) {
                  // Log failure into Failure table
                  Failure.create({
                    worker_id: user.id,
                    type: "Change role",
                    time: new Date()
                  }, function (err, failure) {
                    if (err) throw err;
                    console.log('Failure logged', failure);
                  });
                  console.log(err)
                  res.status(500)
                  res.json({
                    error: "An issue has been encountered when updating worker!",
                    error_status: 500
                  })
                  return;
                } else {
                  res.status(200)
                  res.json({
                    Success: "Role changed"
                  });
                }
              });

            }
          }
          catch (e) {
            console.log(e)
            Failure.create({
              worker_id: user.id,
              type: "Change role",
              time: new Date()
            }, function (err, failure) {
              if (err) throw err;
              console.log('Failure logged', failure);
            });
            res.status(500)
            return;
          }

        });
      }
    });
  });

  // Create change role request for shift manager
  app.put('/changeRoleConfirmation', function (req, res) {

    if (typeof req.body.email == 'undefined' || typeof req.body.requestedRole == 'undefined') {
      res.status(400)
      res.json({
        error: "Request body is not defined properly!",
        error_status: 400
      })
      return;
    }

    // Get user id and user role
    User.findOne({ where: { email: req.body.email } }, (err, user) => {
      if (err || !user) {
        // Set response
        res.status(404)
        res.json({
          error: "The user '" + req.body.email + "' does not exist!",
          error_status: 404
        })
        return;
      } else {

        user.updateAttribute('role', req.body.requestedRole, function (err, instance) {
          if (err || !instance) {
            console.log("Error happened when updating user attribute")
            res.status(500)
            res.json({
              error: "An issue has been encountered when updating worker!",
              error_status: 500
            })
            return;
          } else {
            user.shift(function (err, shift) {
              if (err || !instance) {
                console.log("Error happened when updating shift attribute")
                res.status(500)
                res.json({
                  error: "An issue has been encountered when updating worker!",
                  error_status: 500
                })
                return;
              }
              shift.updateAttribute('role', req.body.requestedRole, function (err, instance) {
                if (err || !instance) {
                  console.log("Error happened when updating shift attribute")
                  res.status(500)
                  res.json({
                    error: "An issue has been encountered when updating worker!",
                    error_status: 500
                  })
                  return;
                }
                res.json({
                  Success: "Role change confirmed"
                })
              })
            })
          }
        });
      }
    });
  });

  app.get('/getNotification', function (req, res) {
    if (typeof req.query.shiftManagerId == 'undefined') {
      res.status(400)
      res.json({
        error: "Request body is not defined properly!",
        error_status: 400
      })
      return;
    }

    User.findOne({ where: { id: req.body.shiftManagerId } }, (err, user) => {
      if (err || !user) {
        // Set response
        res.status(400)
        res.json({
          error: "The user '" + req.body.email + "' does not exist!",
          error_status: 400
        })
        return;
      } else {

        res.status(200)
        res.json(user.NotificationsManager.notifications);
      }
    });
  });

}
