'use strict';

function NotificationsManager() {

  this.notifications = []
  this.initialized = false

  this.create = (managerID, workerID, email, role) => {
    if (!this.initialized) {
      this.initCron()
      this.initialized = true
    }
    this.notifications.push({
      shiftManagerID: managerID,
      workerID: workerID,
      workerEmail: email,
      requestedRole: role,
      time: new Date(),
    })
  }

  this.initCron = () => {
    setInterval(this.removeOutdatedNotification, 1000) //called every second
  }

  this.removeOutdatedNotification = () => {
    if (this.notifications.length !== 0) {
      this.notifications.forEach(function (item, index, array) {
        const curr = new Date();
        const diff = curr - item.time;
        const mnElapsed = Math.round(((diff % 86400000) % 3600000) / 60000);
        if (mnElapsed >= 3) {
          array.splice(index, 1);
        }
      })
    }
  }
}

module.exports = function (Worker) {

  Worker.prototype.NotificationsManager = new NotificationsManager();
  
};
