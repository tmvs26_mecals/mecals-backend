var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.mysql_mecals;
ds.automigrate('Worker', function(err) {
  if (err) throw err;

  ds.disconnect();

});

ds.automigrate('Asmws', function(err) {
  if (err) throw err;

  ds.disconnect();

});

ds.automigrate('Shift', function(err) {
  if (err) throw err;

  ds.disconnect();

});

ds.automigrate('Failure', function(err) {
  if (err) throw err;

  ds.disconnect();

});
